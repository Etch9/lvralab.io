---
weight: 50
title: Easy Anti-Cheat
---

# Easy Anti-Cheat

Sometimes, seemingly randomly, you can get an EAC error during the VRChat startup/login screen. It looks like a message box, with the title "Anti-cheat Error", no actual message text, and a single button labeled "Quit".

## Recommended Workaround

Wrapper script for starting VRChat: [startvrc.sh on GitHub](https://gist.github.com/galister/ca63a5795694e4c839a8e7f4bb8a35bc)


Set startup options for VRChat:
`/path/to/startvrc.sh %command%`

If you're using extra env vars, they should go first:
`PRESSURE_VESSEL_FILESYSTEMS_RW=/run/user/1000/monado_comp_ipc /path/to/startvrc.sh %command%`


## Root Cause

We are still trying to determine the cause.

It is likely caused by a race condition. Closing unnecessary apps or otherwise **lowering your CPU usage** before starting VRChat seems to make the EAC error happen significantly less often. It is safe to crank the CPU usage back up after the home world has loaded.

The following sections do not fix the issue, but are still kept on the wiki to dispel rumors and record research.

## (Outdated) glibc dt_gnu_hash

Previously, we believed it to be related to an incompatible glibc version. However, upon further testing, this turned out to be insufficient.

In other games with EAC, getting past the EAC splash screen had required glibc to be built with a specific flag to work.<sup>[1],[2]</sup> (`--hash-style=both`)

[1]: https://github.com/ValveSoftware/Proton/issues/6051
[2]: https://www.phoronix.com/news/Glibc-2.36-EAC-Problems

Flatpak Steam includes this flag, however Flatpak Steam is not recommended for VR.

As of 2024-02-09, Arch Linux no longer ships with this flag.

- Arch users could use this EAC-specific glibc from the AUR: https://aur.archlinux.org/packages/glibc-eac
- Gentoo users could enable the fix using the `hash-sysv-compat` USE flag on `sys-libs/glibc`.

To reiterate, this glibc flag does _not_ appear to fix VRChat's particular flavor of EAC failures.
