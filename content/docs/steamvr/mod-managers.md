---
weight: 200
title: Using Mod Managers
---

# Using Third-Party Mod Managers with Steam Games

This is a guide on how to install and integrate mod managers (like Vortex, MO2, etc.) for games running in Proton.

**Note:** Most of this stuff will break if you use Flatpak for any of the involved programs.

![skyrim_vr_vortex](/images/steam_launch_vortex.png "Example usage: Vortex Mod Manager integrated into Skyrim VR on Steam")

## Pre-Requisites
- https://github.com/Matoking/protontricks
- https://github.com/tralph3/Steam-Metadata-Editor
- Launch your game at least once before!

## Procedure

1. Download your mod manager installer (Vortex, MO2, etc.)
2. Run the installer with `Protontricks Launcher`, select your game prefix when prompted
3. Finish the installer and note the installation path
4. Symlink mod manager's installation path inside prefix into the game's install path. For example:
```
ln -s "/home/faith/.local/share/Steam/steamapps/compatdata/611670/pfx/drive_c/Program Files/Black Tree Gaming Ltd/Vo
rtex/" "/home/faith/.local/share/Steam/steamapps/common/SkyrimVR/Vortex"
```
5. **Close Steam!** Then open Steam Metadata Editor
6. Find your game in the list, then click `Edit launch menu`
7. Click `Add New Entry`
8. In Description, enter the display name (e.g., `Vortex Mod Manager`)
9. Click on the `...` button in the Executable field
10. File picker will appear, which should be inside the game's install directory. If it's not, try again, restart your computer, or perform some ritual because it sometimes doesn't work properly.
11. Choose the mod manager executable (e.g., `Vortex/Vortex.exe`)
12. Make sure a relative path appears in the Executable field, as above! It won't work otherwise. If you see a full path `/home/faith/.local/share/steam/.../`, try deleting the entry and restarting the metadata editor.
13. The working directory should fill in automatically
14. Close the modal window
15. Click `Save` button
16. Enjoy

## Important notes

* Steam might occasionally remove the custom launch entry, just follow the guide again from Step 5.
* You have to install mod managers and dependencies for each game separately.
* You can't use the Nexus Mods button `Mod Manager Download` via browser, you have to download and adds mods manually.

# Redirecting game executable from custom launchers

It is possible to "redirect" any executable that Steam runs and launch any other executable you specify instead. This can be useful for launching Script Extender loader without having to replace any files.

You need to set game's launch options to the following:

```
bash -c 'exec "${@/OriginalGame.exe/CustomLoader.exe}"' -- %command%
```

replacing `OriginalGame.exe` and `CustomLoader.exe` with game's executable name and your custom launcher respectively.

Here's an example of full Fallout 4 VR launch options:

```
WINEDLLOVERRIDES="WinHTTP.dll=n,b;x3daudio1_7.dll=n,b" bash -c 'exec "${@/Fallout4VR.exe/f4sevr_loader.exe}"' -- %command%
```

Put any environment variables before `bash`, put any launch arguments for the game after `%command%`.