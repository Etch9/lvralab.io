---
weight: 51
title: WlxOverlay-S
---

# WlxOverlay-S

- [WlxOverlay-S GitHub repository](https://github.com/galister/wlx-overlay-s)

WlxOverlay-S is a tool that lets users interact with their X11 or Wayland desktop from inside VR.

It supports a vast variety of desktop environments, and comes with a fully customizable keyboard and watch.

WlxOverlay-S also includes a playspace mover and can act like your home environment (void) when launched as the first app in Monado/WiVRn.

Please take a look at the GitHub Readme for a comprehensive guide.

## WayVR

WayVR acts as a bridge between Wayland applications and WlxOverlay-S panels, allowing you to display your applications within a VR environment. Internally, WayVR utilizes Smithay to run a Wayland compositor.

Read more about setup/usage in the [WayVR README](https://github.com/galister/wlx-overlay-s/tree/main/contrib/wayvr).

## Support

Reach out in the `wlxoverlay` room in Discord or Matrix.