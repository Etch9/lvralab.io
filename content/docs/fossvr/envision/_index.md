---
weight: 49
title: Envision
---

# Envision

<video width="640" height="360" controls>
  <source src="/video/envision_installation/envision_installation.webm" type="video/webm">
</video>

- [Envision GitLab repository](https://gitlab.com/gabmus/envision)

Envision is a graphical app that acts as an orchestrator to get a full [Monado](/docs/fossvr/monado/) or [WiVRn](/docs/fossvr/wivrn/) setup up and running with a few clicks.

Envision attempts to construct a working runtime with both a native OpenXR and an OpenVR API, provided by [OpenComposite](/docs/fossvr/opencomposite/), for client aplications to utilize. Please note the OpenVR implementation is incomplete and contains only what's necessary to run most games for compatibility. If you plan to implement software, utilize the OpenXR API, specification [here](https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html).

{{% hint danger %}}
**Warning**

Envision is still considered alpha-quality and highly experimental.
{{% /hint %}}

You can download the latest AppImage snapshot from [GitLab Pipelines](https://gitlab.com/gabmus/envision/-/pipelines?ref=main&status=success).

If you are on Arch, You can use the [AUR package](https://aur.archlinux.org/packages/envision-xr-git) `envision-xr-git`

## Getting started

Upon launching Envision, you will need to select a profile on the bottom of the left side bar.

Profiles that are available by default:

- Lighthouse driver: Proprietary SteamVR Lighthouse driver with top tracking quality. Recommended for Lighthouse HMDs.
- OpenHMD: Recommended for Oculus CV1 HMDs.
- Simulated headset: Dummy driver for testing on a flat screen.
- Survive: FOSS Lighthouse driver implementation. Lower track quality and less robust than SteamVR proprietary.
- WMR: Use with any SLAM based HMD or Windows Mixed Reality headsets. Inlcudes Rift S support.
- WiVRn: Robust wireless streaming solution for all Android based standalone HMDs.

Monado does not have a launcher app, and so after connecting your headset, you will likely see a solid color. This means you can now start your VR title.

You may want to launch [WlxOverlay-S](/docs/fossvr/wlxoverlay-s/) first and use it to access your desktop and other VR titles. You can even start it automatically alongside the Monado/WiVRn session, by enabling it in the [Plugins](#plugin-system) menu.

## Plugin system

The plugin system for Envision allows you to launch anything (usually OpenXR overlays) alongside the Monado/WiVRn session.

Open the Plugins window by clicking the kebab menu (three dots) at the top of the Envision window, and clicking "Plugins". Here, you can install overlays like [WlxOverlay-S](/docs/fossvr/wlxoverlay-s/) or [Stardust XR](/docs/fossvr/stardust/) (or a custom plugin &mdash; see below). After installing plugins and toggling them on, once you start a profile, the enabled plugins will start alongside it.

Plugins are the replacement for the "autostart" feature from older versions.

### Adding a custom plugin

The plugins included in the store are just a quick front-end to the latest release AppImages. For everything else, you can create a custom plugin that runs any given executable script or binary.

For example, if you installed WlxOverlay-S from AUR or built it from source, you can disable the original WlxOverlay-S plugin and create a custom plugin to run it:

1. Create a new file `~/.local/bin/wlx-but-newer` (creating the `~/.local/bin` directory if it does not exist).
2. Give it the content:
    ```bash
    #!/usr/bin/env bash
    /usr/bin/wlx-overlay-s --openxr
    ```
3. `chmod +x ~/.local/bin/wlx-but-newer`
4. In Envision, open the Plugins window.
5. Add a custom plugin by clicking the &#x2795; plus button.
6. For the executable file, point it to the script you just created.
7. Name the plugin "Wlx but newer".
8. Save, and then enable the new plugin.

Now starting the profile will also start your custom or AUR version of WlxOverlay-S.

## Experimental feature settings

The following resources can be entered into your Envision profile repo and branch settings to enable early access to code before it's fully upstream in Monado itself. To enable these feature sets, simply clone your profile, edit it with these settings, then build.

### Full body Lighthouse tracking

Full body is no longer experimental; it is enabled by default in Monado & OpenComposite through the XR_MNDX_xdev_space OpenXR vendor extension. This allows any tracked "xdev" in Monado to be forwarded as a raw pose without bindings to applications. OpenComposite exposes these as fake Vive FBT trackers for use.

### WMR & Rift S controller tracking

This enables positional tracking for WMR controllers in full 6dof.
Includes Rift S support & controllers.

For the Envision XR Service settings:

- Repo: `https://gitlab.freedesktop.org/thaytan/monado`
- Branch: `dev-constellation-controller-tracking`

### Experimental Pimax support branch

Modest WIP. Allows certain Pimax HMDs to function with Monado. Look Ma, no Pitools!

For the Envision XR Service settings:

- Repo: `https://gitlab.freedesktop.org/Coreforge/monado/`
- Branch: `pimax`

Dumped distortion parameters are found here, please follow the instructions in the README.md and consult with LVRA to add your own if missing.

- https://gitlab.freedesktop.org/othello7/pimax-distortion/

### OpenHMD Rift CV1 support

The Oculus Rift CV1 is supported on the OpenHMD profile of Envision.

Start the profile with your headset placed on the floor in clear view of all bases to generate this file as your calibrated playspace origin first run and delete it to reset the configuration.

A calibration of base stations will be saved to disk at `~/.config/openhmd/rift-room-config.json`.
