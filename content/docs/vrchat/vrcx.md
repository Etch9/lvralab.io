---
weight: 800
title: VRCX
---

# VRCX
- [VRCX GitHub Repository](https://github.com/vrcx-team/VRCX)

VRCX is a tool for managing your VRChat friendships, as well as providing additional convenience functionalities.

# Installer Script

VRCX provides the [install-vrcx.sh](https://github.com/vrcx-team/VRCX/blob/master/Linux/install-vrcx.sh) script upstream, which does the steps of the manual installation for you.

You can run it per `curl -sSf https://raw.githubusercontent.com/vrcx-team/VRCX/master/Linux/install-vrcx.sh | bash`.


# Manual Installation

- Grab the latest .zip from here: [VRCX Official Releases](https://github.com/vrcx-team/VRCX/releases)
- Use a new or existing Wine prefix of Wine 9.2 or later. Recommend using a non-proton Wine build.
- `winetricks corefonts`
- Symlink your `drive_c/users/steamuser/AppData/LocalLow/VRChat/VRChat` folder from the VRChat Wine prefix to the VRCX Wine prefix
- Run `VRCX.exe` via Wine.